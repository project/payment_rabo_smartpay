<?php

/**
 * @file
 * Hook documentation.
 */

/**
 * Responds to Rabo Smartpay payment feedback.
 *
 * @param array $data
 *   The data.
 * @param Payment $payment
 *   The payment.
 */
function hook_payment_rabo_smartpay_feedback(array $data, Payment $payment) {
  if ($data['AMOUNT'] < $payment->totalAmount(TRUE)) {
    \Drupal::messenger()->addMessage(t('The payment is not sufficient.'));
  }
  else {
    \Drupal::messenger()->addMessage(t('Thank you!'));
  }
}
