<?php

namespace Drupal\payment_rabo_smartpay\Plugin\Payment\Method;

use Drupal\payment\Plugin\Payment\Method\PaymentMethodConfigurationOperationsProvider;

/**
 * Provides Rabo Smartpay operations based on config entities.
 */
class PaymentRaboSmartpayPaymentOperationsProvider extends PaymentMethodConfigurationOperationsProvider {

  /**
   * {@inheritdoc}
   */
  protected function getPaymentMethodConfiguration($plugin_id) {
    $offset = strpos($plugin_id, ":") + 1;
    $entity_id = substr($plugin_id, $offset);

    return $this->paymentMethodConfigurationStorage->load($entity_id);
  }

}
