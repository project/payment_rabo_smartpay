<?php

namespace Drupal\payment_rabo_smartpay\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines Rabo Smartpay profiles.
 */
interface PaymentRaboSmartpayProfileInterface extends ConfigEntityInterface {

}
