<?php

namespace Drupal\payment_rabo_smartpay\Entity\PaymentRaboSmartpayProfile;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\plugin\Plugin\Plugin\PluginSelector\PluginSelectorManagerInterface;
use Drupal\plugin\PluginType\PluginTypeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use nl\rabobank\gict\payments_savings\omnikassa_sdk\model\Environment;

/**
 * Provides the Rabo Smartpay profile add/edit form.
 */
class PaymentRaboSmartpayProfileForm extends EntityForm {

  /**
   * The Rabo Smartpay profile storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $paymentRaboSmartpayProfileStorage;

  /**
   * The plugin selector manager.
   *
   * @var \Drupal\plugin\Plugin\Plugin\PluginSelector\PluginSelectorManagerInterface
   */
  protected $pluginSelectorManager;

  /**
   * The plugin type manager.
   *
   * @var \Drupal\plugin\PluginType\PluginTypeManager
   */
  protected $pluginTypeManager;

  /**
   * Constructs a new instance.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translator.
   * @param \Drupal\Core\Entity\EntityStorageInterface $payment_rabo_smartpay_profile_storage
   *   The Rabo Smartpay profile storage.
   * @param \Drupal\plugin\Plugin\Plugin\PluginSelector\PluginSelectorManagerInterface $plugin_selector_manager
   *   The plugin selector manager.
   * @param \Drupal\plugin\PluginType\PluginTypeManager $plugin_type_manager
   *   The plugin type manager.
   */
  public function __construct(TranslationInterface $string_translation, EntityStorageInterface $payment_rabo_smartpay_profile_storage, PluginSelectorManagerInterface $plugin_selector_manager, PluginTypeManager $plugin_type_manager) {
    $this->paymentRaboSmartpayProfileStorage = $payment_rabo_smartpay_profile_storage;
    $this->pluginSelectorManager = $plugin_selector_manager;
    $this->pluginTypeManager = $plugin_type_manager;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');

    return new static($container->get('string_translation'), $entity_type_manager->getStorage('payment_rabo_smartpay_profile'), $container->get('plugin.manager.plugin.plugin_selector'), $container->get('plugin.plugin_type_manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\payment_rabo_smartpay\Entity\PaymentRaboSmartpayProfileInterface $payment_rabo_smartpay_profile */
    $payment_rabo_smartpay_profile = $this->getEntity();

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#default_value' => $payment_rabo_smartpay_profile->label(),
      '#maxlength' => 255,
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#default_value' => $payment_rabo_smartpay_profile->id(),
      '#disabled' => !$payment_rabo_smartpay_profile->isNew(),
      '#machine_name' => [
        'source' => ['label'],
        'exists' => [$this, 'paymentRaboSmartpayProfileIdExists'],
      ],
      '#maxlength' => 255,
      '#type' => 'machine_name',
      '#required' => TRUE,
    ];
    $form['signingToken'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Token'),
      '#description' => $this->t('Enter the token from your Dashboard.'),
      '#default_value' => $payment_rabo_smartpay_profile->getSigningToken(),
      '#required' => TRUE,
    ];
    $form['signingKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Signing key'),
      '#description' => $this->t('The signing key from your Dashboard.'),
      '#default_value' => $payment_rabo_smartpay_profile->getSigningKey(),
      '#required' => TRUE,
    ];
    $form['ipn_url'] = [
      '#markup' => $this->t(
        '<div>
<strong>Webhook:</strong><br/>Rabo Smartpay can send you notifications if the
status of a payment changes. Then the payment-status in the site is updated
to match the status at the provider.<br/>The url for the webhook is:<br/>
<em>@webhook_url</em><br/>
You can configure this in the Rabo Smartpay-control panel.</div>',
        ['@webhook_url' => Url::fromRoute('payment_rabo_smartpay.redirect')->setAbsolute(TRUE)->toString()]
      ),
      '#allowed_tags' => ['div'],
    ];
    $payment_types_class = new \ReflectionClass('nl\rabobank\gict\payments_savings\omnikassa_sdk\model\PaymentBrand');
    $payment_types = $payment_types_class->getConstants();
    $payment_types['OMNIKASSA'] = 'EXTERNAL';
    $form['paymentTypes'] = [
      '#type' => 'radios',
      '#options' => $payment_types,
      '#title' => $this->t('What paymenttypes do you accept?'),
      '#description' => $this->t('Select the payment option that can be used.
      <ul><li>The option \'CARDS\' will enable all card-types:<br/>
      MASTERCARD, VISA, BANCONTACT, MAESTRO and V_PAY.<br/>
      The right card is chosen at the Payment provider.</li>
      <li>The option \'EXTERNAL\' will skip all options above.<br/>
      The payment option will be chosen at the Payment provider.</li></ul>'),
      '#default_value' => $payment_rabo_smartpay_profile->getPaymentTypes(),
      '#required' => TRUE,
    ];
    $form['serverTypes'] = [
      '#type' => 'select',
      '#title' => $this->t('API URL'),
      '#default_value' => $payment_rabo_smartpay_profile->getServerType(),
      '#options' => [
        Environment::PRODUCTION => $this->t('Production'),
        Environment::SANDBOX => $this->t('Testing/Sandbox'),
      ],
      '#required' => TRUE,
    ];

    return parent::form($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    /** @var \Drupal\payment_rabo_smartpay\Entity\PaymentRaboSmartpayProfileInterface $entity */
    parent::copyFormValuesToEntity($entity, $form, $form_state);
    $values = $form_state->getValues();
    $entity->setId($values['id']);
    $entity->setLabel($values['label']);
    $entity->setSigningToken($values['signingToken']);
    $entity->setSigningKey($values['signingKey']);
    $entity->setPaymentTypes($values['paymentTypes']);
    $entity->setServerType($values['serverTypes']);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $payment_rabo_smartpay_profile = $this->getEntity();
    $status = $payment_rabo_smartpay_profile->save();
    $this->messenger()->addStatus($this->t('@label has been saved.', [
      '@label' => $payment_rabo_smartpay_profile->label(),
    ]));
    $form_state->setRedirect('entity.payment_rabo_smartpay_profile.collection');
    return $status;
  }

  /**
   * Checks if a Rabo Smartpay profile with a particular ID already exists.
   *
   * @param string $id
   *   The profile.
   *
   * @return bool
   *   Whether the profile exists.
   */
  public function paymentRaboSmartpayProfileIdExists($id) {
    return (bool) $this->paymentRaboSmartpayProfileStorage->load($id);
  }

}
